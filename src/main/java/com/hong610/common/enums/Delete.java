package com.hong610.common.enums;

/**
 * 删除
 * Created by Hong on 2016/11/28.
 */
public enum Delete {
    UNDELETE(false, "未删除"),
    DELETE(true, "删除");


    private boolean value;

    private String text;

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    Delete(boolean value, String text) {
        this.value = value;
        this.text = text;
    }
}
